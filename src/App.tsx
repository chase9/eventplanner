import {
  ActionButton,
  DatePicker,
  DefaultButton,
  getTheme,
  initializeIcons,
  IStackTokens,
  Label,
  Stack,
  TextField, TimePicker
} from '@fluentui/react';
import ical, { ICalEventData } from 'ical-generator';
import { DateTime } from 'luxon';
import React from 'react';
import './App.css';

// We need to initialize icons in order to see/use them
initializeIcons();

const theme = getTheme();
const stackTokens: IStackTokens = {
  padding: 10,
  childrenGap: 10
};

interface EventsPlannerProps {
  events: ICalEventData[];
}

interface EventsPlannerState {
  events: ICalEventData[];
}

class EventPlanner extends React.Component<EventsPlannerProps, EventsPlannerState> {
  constructor(props: EventsPlannerProps) {
    super(props);

    this.state = {
      events: props.events
    };
  }

  removeEvent(eventId: string | number | null | undefined) {
    const updatedEventList = [...this.state.events];
    updatedEventList.splice(updatedEventList.findIndex(e => e.id === eventId), 1);

    this.setState({
      events: updatedEventList
    });
  };

  render() {
    return (
      <Stack tokens={stackTokens}>
        {this.state.events.map((event: ICalEventData) => {
          return (
            <Stack style={{ boxShadow: theme.effects.elevation4 }} horizontal={false} verticalAlign={'center'}>
              <Event key={event.id} event={event}/>
              <ActionButton iconProps={{ iconName: 'Trash' }} onClick={() => this.removeEvent(event.id)}>
                Remove Event
              </ActionButton>
            </Stack>);
        })}

        <Stack horizontal={true} verticalAlign={'center'}>
          <ActionButton
            iconProps={{ iconName: 'Add' }}
            onClick={() => {
              this.setState({
                events: this.state.events.concat({
                  summary: '',
                  start: DateTime.local().startOf('day'),
                  end: DateTime.local().plus({ day: 1 }).startOf('day')
                })
              });
            }}
          >
            Add Event
          </ActionButton>
          <DefaultButton
            onClick={async () => GenerateEvents(this.state.events!)}
          >
            Generate iCal
          </DefaultButton>
        </Stack>
      </Stack>
    );
  }
}

interface EventContainer {
  event: ICalEventData;
}

function Event(props: EventContainer) {
  const updateDate = (newDate: DateTime | Date | undefined | null, dateType: 'start' | 'end') => {
    if (newDate === null || newDate === undefined) {
      return;
    }
    // If newDate is a Date, convert to DateTime
    const convertedDate = newDate instanceof DateTime ? newDate : DateTime.fromJSDate(newDate);

    switch (dateType) {
      case 'start':
        props.event.start = props.event.start.set({
          year: convertedDate.get('year'),
          month: convertedDate.get('month'),
          day: convertedDate.get('day')
        });
        break;
      case 'end':
        props.event.end = props.event.end.set({
          year: convertedDate.get('year'),
          month: convertedDate.get('month'),
          day: convertedDate.get('day')
        });
        break;
    }
  };

  const updateTime = (timeType: 'start' | 'end', newTime?: string) => {
    if (newTime === undefined) {
      return;
    }
    // If newTime is a Date, convert to DateTime
    const convertedTime = DateTime.fromFormat(newTime, 't');

    switch (timeType) {
      case 'start':
        props.event.start = props.event.start.set({
          hour: convertedTime.get('hour'),
          minute: convertedTime.get('minute')
        });
        break;
      case 'end':
        props.event.end = props.event.end.set({
          hour: convertedTime.get('hour'),
          minute: convertedTime.get('minute')
        });
        break;
    }
  };

  return (
    <form>
      <Stack tokens={stackTokens}>
        <div>
          <Label htmlFor="title">Title</Label>
          <TextField
            id="title"
            defaultValue={props.event.summary}
            onChange={(_, newTitle) => props.event.summary = newTitle}
            placeholder="Title"/>
        </div>
        <Stack horizontal={true} tokens={{ childrenGap: 10 }}>
          <Stack.Item grow={5}>
            <DatePicker
              value={props.event.start.toJSDate()}
              onSelectDate={(date => updateDate(date, 'start'))}
            />
          </Stack.Item>
          <Stack.Item grow={1}>
            <TimePicker
              useHour12
              dropdownWidth={100}
              onChange={(_, option) =>
                updateTime('start', option?.text || undefined)}
            />
          </Stack.Item>
        </Stack>
        <Stack horizontal={true} tokens={{ childrenGap: 10 }}>
          <Stack.Item grow={5}>
            <DatePicker
              value={props.event.end.toJSDate()}
              onSelectDate={date => updateDate(date, 'end')}
            />
          </Stack.Item>
          <Stack.Item grow={1}>
            <TimePicker
              useHour12
              dropdownWidth={100}
              onChange={(_, option) =>
                updateTime('end', option?.text || undefined)}

            />
          </Stack.Item>
        </Stack>
      </Stack>
    </form>
  );
}

function GenerateEvents(events: ICalEventData[]) {
  const calendar = ical({
    name: 'EventPlanner'
  });

  events.forEach(event => {
    calendar.createEvent(event);
  });

  window.location.href = calendar.toURL();
}

export default EventPlanner;
