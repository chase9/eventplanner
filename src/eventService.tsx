import ical from 'ical-generator';
import { DateTime } from 'luxon';

const calendar = ical({ name: 'Test iCal' });
calendar.createEvent({
  start: DateTime.now(),
  end: DateTime.now().plus({ days: 1 }),
  summary: 'Example',
  description: 'This is a description',
  location: '2115 Summit Ave., Saint Paul, MN, 55105',
  url: 'https://localhost'
});

let saveUrl = calendar.toURL()
console.log(saveUrl);
