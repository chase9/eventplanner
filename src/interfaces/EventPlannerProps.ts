import { ICalEventData } from 'ical-generator';

export interface EventPlannerProps {
  events?: ICalEventData[];
}
