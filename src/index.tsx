import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import '@fluentui/react/dist/css/fabric.css';
import EventPlanner from './App';
import reportWebVitals from './reportWebVitals';
import { DateTime } from 'luxon';
import { ICalEventData } from 'ical-generator';
import { Stack } from '@fluentui/react';


ReactDOM.render(
  <React.StrictMode>
    <header>
      <Stack verticalAlign={'center'}>
        <p>EventPlanner</p>
      </Stack>
    </header>
    <EventPlanner events={[
      {
        id: 1,
        summary: 'Example Event',
        start: DateTime.local().startOf('day'),
        end: DateTime.local().plus({ day: 1 }).startOf('day')
      },
      {
        id: 2,
        summary: 'Example Event 2',
        start: DateTime.local().startOf('day'),
        end: DateTime.local().plus({ day: 1 }).startOf('day')
      }
    ] as ICalEventData[]}/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
